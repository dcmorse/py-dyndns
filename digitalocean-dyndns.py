#!/usr/bin/env python3
from requests import Session
from enum import Enum
import argparse
from pathlib import Path
import datetime as dt
import logging
from typing import NamedTuple, List, Optional, Dict, Any
import configparser
import re
import os
from collections import defaultdict

__version__ = '0.0.1'
DO_API_BASE = 'https://api.digitalocean.com/v2'
EXTERNAL_IPV4_TEST_URL = 'https://dynamicdns.park-your-domain.com/getip'
EXTERNAL_IPV6_TEST_URL = 'http://ip6.iurl.no'
DEFAULT_APP_DIR = Path('~').expanduser() / '.digitalocean-dyndns'
DEFAULT_CONFIG_FILE = DEFAULT_APP_DIR / 'domains.conf'
DEFAULT_LOG_FILE = DEFAULT_APP_DIR / 'update.log'
APPLICATION_JSON = 'application/json'
LOGGER = logging.getLogger('digitalocean-dyndns')


class DNSRecords(Enum):
    A = 'A'
    AAAA = 'AAAA'
    # TXT = 'TXT'
    # MX = 'MX'
    # NS = 'NS'


class DigitalOceanDomainRecord(NamedTuple):
    id: int
    type: str  # DNSRecords
    name: str
    data: str
    priority: Optional[int]
    port: Optional[int]
    ttl: int
    weight: Optional[str]
    flags: Optional[str]
    tag: Optional[str]


class DigitalOceanDomainRecordList(NamedTuple):
    domain_records: List[DigitalOceanDomainRecord]
    links: Dict[str, Dict[str, str]]
    meta: Dict[str, int]


class IPHistoryItem(NamedTuple):
    date: dt.datetime
    host: str
    record_type: DNSRecords
    ipv4: str
    ipv6: str


class SubdomainConfiguration(NamedTuple):
    api_key: str
    logfile: str
    base_domain: str
    subdomain: str
    ipv4: Optional[str]
    ipv6: Optional[str]
    ttl: Optional[int]
    src_file: Optional[str]


def parse_commandline() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='DigitalOcean Dynamic DNS')
    # parser.add_argument(
    #     '-V', '--version',
    #     action='version',
    #     version=f'%(prog)s {__version__}',
    # )
    # parser.add_argument(
    #     '-v', '--verbose',
    #     help='Verbosity',
    #     required=False,
    #     action='count',
    # )
    # parser.add_argument(
    #     '-b', '--base-domain',
    #     help='Domain to update',
    #     required=False,
    #     action='store',
    # )
    # parser.add_argument(
    #     '-s', '--subdomain',
    #     help='Domain to update',
    #     required=False,
    #     action='append',
    # )
    # parser.add_argument(
    #     '-p', '--persist',
    #     help='Save to config file',
    #     required=False,
    #     action='store_true',
    # )
    parser.add_argument(
        '-f', '--configfile',
        help='Save to config file',
        required=False,
        default=str(DEFAULT_CONFIG_FILE),
        action='store',
    )
    parser.add_argument(
        '-l', '--logfile',
        help='Log file',
        required=False,
        default=str(DEFAULT_LOG_FILE),
        action='store',
    )
    parser.add_argument(
        '-d', '--dry-run',
        help="Don't actually update anything",
        required=False,
        action='store_true',
    )
    return parser.parse_args()


def parse_configfile(config_filepath: str, **kwargs) -> Dict[str, SubdomainConfiguration]:
    conf = configparser.ConfigParser()
    with open(config_filepath, 'r', encoding='utf_8') as f:
        configstr = f.read()
    if not re.search(r'^\s*\[DEFAULT\]', configstr):
        configstr = f'[DEFAULT]\n{configstr}'
    conf.read_string(configstr)

    defaults_dict = {}
    configs: Dict[str, SubdomainConfiguration] = {}
    default_fields = ['api_key', 'logfile', 'base_domain']

    for field in default_fields:
        defaults_dict[field] = conf['DEFAULT'].get(field, kwargs.get(field))

    include_field = conf['DEFAULT'].get('include')
    if include_field:
        include_path = Path(include_field)
        included_files = []
        if not include_path.is_absolute():
            include_path = (Path(config_filepath).parent / include_path).resolve()
        if include_path.is_dir():
            included_files += list(include_path.iterdir())
        elif include_path.is_file():
            included_files.append(include_path)
        elif include_path.name:  # maybe its glob
            included_files += list(include_path.parent.glob(include_path.name))
        for ifile in included_files:
            configs.update(parse_configfile(str(ifile), **defaults_dict))

    for sdom in conf.sections():
        this_config = dict(
            api_key=conf[sdom].get('api_key', defaults_dict.get('api_key')),
            logfile=conf[sdom].get('logfile', defaults_dict.get('logfile')),
            base_domain=conf[sdom].get('base_domain', defaults_dict.get('base_domain')),
            subdomain=sdom,
            ipv4=conf[sdom].get('ipv4') or None,
            ipv6=conf[sdom].get('ipv6') or None,
            ttl=(
                lambda x: int(x) if x else 0
            )(conf[sdom].get('ttl')),
            src_file=config_filepath,
        )
        prev_config = configs.get(sdom) or {}
        merged_config = {}
        for key in this_config.keys():
            this_config_val = this_config.get(key)
            merged_config[key] = prev_config.get(key) if this_config_val is None else this_config_val
        configs.update({sdom: SubdomainConfiguration(**merged_config)})

    return configs


# def build_unified_config(cmdline_conf: argparse.Namespace,
#                          file_conf: Dict[str, SubdomainConfiguration]
#                          ) -> List[SubdomainConfiguration]:
#     ...


def fetch_dns_records(sess: Session, base_domain: str) -> Dict[str, Dict[str, Dict[str, Any]]]:
    resp = sess.get(f'{DO_API_BASE}/domains/{base_domain}/records/')
    resp.raise_for_status()
    resp_dict = resp.json()
    # domain_records = []
    # for record in resp_dict['domain_records']:
    #     domain_records.append(DigitalOceanDomainRecord(**record))
    # links = resp_dict.get('links')
    # meta = resp_dict.get('meta')
    domain_records = resp_dict['domain_records']
    domain_records_new = {}
    for record in domain_records:
        if record['type'] in ('A', 'AAAA'):
            subdomain_only = record['name']
            if not domain_records_new.get(record['type']):
                domain_records_new[record['type']] = {}
            domain_records_new[record['type']][subdomain_only] = record
    resp_dict['domain_records'] = domain_records_new
    return resp_dict


def fetch_subdomain_record(sess: Session, base_domain: str, subdomain_id: int) -> DigitalOceanDomainRecord:
    resp = sess.get(f'{DO_API_BASE}/domains/{base_domain}/records/{subdomain_id}')
    resp.raise_for_status()
    resp_dict = resp.json()
    return DigitalOceanDomainRecord(**resp_dict['domain_record'])


def setup_http_session(api_key: str, content_type: str = APPLICATION_JSON) -> Session:
    sess = Session()
    sess.headers = {
        'Authorization': f'Bearer {api_key}',
        'Content-Type': content_type
    }
    return sess


def get_current_ip(sess: Session, test_url: str) -> str:
    resp = sess.get(test_url)
    resp.raise_for_status()
    return resp.text.strip()


def update_dns_record(sess: Session,
                      base_domain: str,
                      subdomain_id: int,
                      ipaddress: str) -> Any:
    resp = sess.put(
        url=f'{DO_API_BASE}/domains/{base_domain}/records/{subdomain_id}',
        json={'data': ipaddress},
    )
    resp.raise_for_status()
    return resp.json()


def create_dns_record(sess: Session,
                      record_type: DNSRecords,
                      base_domain: str,
                      subdomain: str,
                      ipaddress: str,
                      ttl: int) -> Dict[Any, Any]:
    resp = sess.post(
        url=f'{DO_API_BASE}/domains/{base_domain}/records/',
        json={
            'name': f'{subdomain}.{base_domain}',
            'data': ipaddress,
            'type': record_type.value,
            'ttl': ttl
        },
    )
    resp.raise_for_status()
    return resp.json()


if __name__ == '__main__':

    DEFAULT_APP_DIR.mkdir(parents=False, exist_ok=True)
    commandline_config = parse_commandline()
    configfile_configs = parse_configfile(commandline_config.configfile)
    # config = build_unified_config(commandline_config, configfile_configs)

    logfile = Path(commandline_config.logfile)
    if not logfile.is_absolute():
        logfile = (Path(os.getcwd()) / logfile).resolve()
    logfile_handler = logging.FileHandler(str(logfile))
    LOGGER.addHandler(logfile_handler)

    dns_records: Dict[str, Dict[str, Dict[str, Dict[str, Any]]]] = {}
    for subd, config in configfile_configs.items():

        session = setup_http_session(config.api_key)
        records = dns_records.get(config.base_domain)

        if not records:
            records = fetch_dns_records(session, config.base_domain)['domain_records']
            dns_records.update({config.base_domain: records})

        prev_ipv4 = config.ipv4
        curr_ipv4 = get_current_ip(session, EXTERNAL_IPV4_TEST_URL)
        if not prev_ipv4 or prev_ipv4 != curr_ipv4:
            if config.subdomain in records['A'].keys():
                LOGGER.info(f'Setting {"A"} record for {subd}.{config.base_domain} to {curr_ipv4}.')
                if not commandline_config.dry_run:
                    update_dns_record(session, config.base_domain, records['A']['id'], curr_ipv4)
