# PY-DYNDNS
PY-DYNDNS is a small Python app that updates DNS records on you Digital Ocean account.
This app is not as polished as its NodeJs counterpart [DYNDNS](https://gitlab.no/rune/dyndns), amongst other things you have to be able to find the domain id(s) of the domain(s) you wish to update.

### Installation
Clone this repo with `git clone https://gitlab.no/rune/py-dyndns.git`. To get your API key, log in to your Digital Ocean account and click the API link at the top of the page. Follow the instructions!  Edit the `config.json` file with your information.

Rename the `config-template.ini` to  `config.ini` and fill in the values needed.

This is made based on Python 3 so you need Python3 to use it! 
### Use
Change permission of the file to be excecutable `chmod +x py-dyndns.py` and run it with `./py-dyndns.py`

* To list all subdomains;  ./py-dyndns.py -l
* To show current IP's for domains in config.ini; ./py-dyndns.py -c
* To add a new domain; ./py-dyndns.py -a `domainname`
* To show help; ./py-dyndns.py -h

To update all subdomains in config.ini run the app without arguments. 

The best way to use this is with cron. To run the app every second hour create a cron job like this:

`0 1-23/2 * * * py-dyndns.py >/dev/null 2>&1`

### Development
* Add IPv6
* ~~Add possibility to list sub domain(s) with domainid(s)~~
* ~~Add help text~~
* ~~Add possibility to update more than one domain~~ 


