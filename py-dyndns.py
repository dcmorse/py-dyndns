#!/usr/bin/env python3
import configparser
import json
import logging
import argparse
import requests as rq

DO_API_BASE = 'https://api.digitalocean.com/v2'
APPLICATION_JSON = 'application/json'

CONFIG_FILE = 'domains.conf'
config = configparser.ConfigParser()
config.read(CONFIG_FILE)

api = config['DYNDNS']['api_key']

IP4_TEST_URL = config['DYNDNS']['ip4']
LOCAL_IP4 = None

IP6_TEST_URL = config['DYNDNS']['ip6']
LOCAL_IP6 = None

DOMAIN_NAME = config['DYNDNS']['baseuri']
AUTH_HEADERS = {
    'Authorization': f'Bearer {api}',
    'Content-Type': APPLICATION_JSON
}
logging.basicConfig(
    filename=config['DYNDNS']['logfile'],
    level=logging.INFO
)


class PyDynDnsException(Exception):
    pass


def get_local_ip(ipv6=False):
    if ipv6:
        global LOCAL_IP6
        if LOCAL_IP6 is None:
            LOCAL_IP6 = rq.get(IP6_TEST_URL).text
        return LOCAL_IP6
    else:
        global LOCAL_IP4
        if LOCAL_IP4 is None:
            LOCAL_IP4 = rq.get(IP4_TEST_URL).text
        return LOCAL_IP4


def get_remote_record(domain_name, subdomain_id):
    return rq.get(
        url=f'{DO_API_BASE}/domains/{domain_name}/records/{subdomain_id}',
        headers=AUTH_HEADERS
    )


def set_remote_record(domain_name, subdomain_id, json_body, http_put=False):
    method = 'put' if http_put else 'post'
    return rq.request(
        method=method,
        url=f'{DO_API_BASE}/domains/{domain_name}/records/{subdomain_id}',
        json=json_body,
        headers=AUTH_HEADERS
    )


def updateip(ip, domain):
    resp = set_remote_record(DOMAIN_NAME, domain, {'data': ip}, http_put=True)
    return resp.status_code


def createnewdomain(name, ip):
    data = {
        'name': name[0],
        'data': ip,
        'type': 'A',
        'ttl': 3600
    }
    resp = set_remote_record(DOMAIN_NAME, domain, data)
    if resp.status_code == 201:
        return resp.json()
    else:
        raise PyDynDnsException("Invalid return code {} when creating new A record")


def runupdate():
    for section in config.sections():
        if not config.has_option(section, 'subdomainid'):
            continue
        for name, value in config.items(section):
            remote_data = get_remote_record(DOMAIN_NAME, value).json()
            remote_ip4 = remote_data['domain_record']['data']
            if remote_ip4 != get_local_ip():
                resp_code = updateip(get_local_ip(), value)
                if resp_code == 200:
                    logging.info(f'Success! Domain {section} updated with IP: {remote_ip4}')
                else:
                    logging.error(f'Failure! Couldn\'t update A record for {name}.{DOMAIN_NAME}')
            else:
                logging.info(
                    f'No need to update the IP! Local IP ({local_ip}) for {section} is the same as remote IP ({remote_ip4})'
                )


def listdomains():
    remoteData = get_remote_record(DOMAIN_NAME, '').json()
    for k in remoteData['domain_records']:
        print("Name: {name}\n\tId: {id}".format(**k))
        if k['type'] == 'A':
            print('\tIPv4: {data}'.format(**k))
        elif k['type'] == 'AAAA':
            print('\tIPv6: {data}'.format(**k))


def listcurrent():
    for section in config.sections():
        if not config.has_option(section, 'subdomainid'):
            continue
        for name, value in config.items(section):
            remote_data = get_remote_record(DOMAIN_NAME, value).json()
            print(remote_data)
            remote_ip4 = remote_data['domain_records']['data']
            print(f'{section}: {remote_ip4}')


def addnewdomin(newdata):
    currentlocalip = get_local_ip()
    response = createnewdomain(newdata['add'][0], currentlocalip)
    if response is not 'Fail':
        logging.info(f'Success! Domain {newdata["add"][0]} updated with ID: {response["domain_record"]["id"]}')
        domainid = str(response['domain_record']['id'])
        section = str(newdata['add'][0])
        section = section.strip("[']")
        config['DYNDNS'][section] = {
            'subdomainid': str(domainid)
        }
        with open(CONFIG_FILE, 'w') as f:
            config.write(f)
            print(f'Domain {section} added with ID {domainid} and {CONFIG_FILE} updated!')
    else:
        print(f'ERROR! Domain "{newdata["add"][0][0]}" not added!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='DigitalOcean Dynamic DNS')
    parser.add_argument(
        '-l', '--list',
        help=f'List the available sub-domains on the domain supplied in the file "{CONFIG_FILE}"',
        required=False,
        action='store_true'
    )
    parser.add_argument(
        '-c', '--current',
        help=f'List the current IP address for the sub-domains in the file "{CONFIG_FILE}"',
        required=False,
        action='store_true'
    )
    parser.add_argument(
        '-a', '--add',
        help=f'Add a new domain to your DigitalOcean account and to the file "{CONFIG_FILE}"',
        required=False,
        nargs=1,
        action='append'
    )
    parser.add_argument(
        '-u', '--update',
        help=f'Uses info from the file "{CONFIG_FILE}" to update your DigitalOcean A records with the current IP.',
        required=False,
        action='store_true'
    )
    args = vars(parser.parse_args())
    if args['list']:
        listdomains()
    elif args['current']:
        listcurrent()
    elif args['add']:
        addnewdomin(args)
    elif args['update']:
        runupdate()
    else:
        print(parser.format_help())
